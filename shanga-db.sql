BEGIN TRANSACTION;
CREATE TABLE users (id INTEGER PRIMARY KEY AUTOINCREMENT, nickName STRING, email STRING, password STRING, hasSubscribed BOOLEAN);
CREATE TABLE review (id INTEGER PRIMARY KEY AUTOINCREMENT, author STRING, body STRING, fk_IdProduct INTEGER, FOREIGN KEY(fk_IdProduct) REFERENCES product(id));
CREATE TABLE product (id INTEGER PRIMARY KEY AUTOINCREMENT, itemName STRING, description STRING, price FLOAT, imagePath STRING);
CREATE TABLE news (id INTEGER PRIMARY KEY AUTOINCREMENT, title STRING, body STRING);
COMMIT;
