package eu.shanga.common;

/**
 * Created by Christophine on 20/12/2015.
 */
public enum SessionStatus {
    RegisterSuccess("Vous vous êtes enregistré avec succès. Vous pouvez vous connecter !", "green"),
    UserDoesNotExist("Le nom et/ou le mot de passe spécifié ne correspond à aucun utilisateur.", "red"),
    DidNotLogIn("Vous n'êtes pas connecté.", "red"),
    None("", "")
    ;

    public final String _displayColor;
    public final String _displayString;
    SessionStatus(String displayString, String displayColor)
    {
        this._displayString = displayString;
        this._displayColor = displayColor;
    }
}
