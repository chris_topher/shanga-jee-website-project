package eu.shanga.common;

/**
 * Created by Christophine on 13/12/2015.
 */

import eu.shanga.viewModels.NewsViewModel;
import eu.shanga.viewModels.ProductItemViewModel;
import eu.shanga.viewModels.UsersViewModel;
import eu.shanga.viewModels.ReviewViewModel;

import java.sql.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ShangaDbContext
{
    protected Connection connection;

    protected void databaseDriverLoad()
    {
        try
        {
            Class.forName("org.sqlite.JDBC");
        }
        catch (ClassNotFoundException e)
        {
            e.printStackTrace();
        }
    }
    protected void initConnection()
    {
        try
        {
            this.connection = DriverManager.getConnection("jdbc:sqlite:shanga-db.db");
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }
    protected void createTables()
    {
        try
        {
            Statement statement = this.connection.createStatement();
            statement.executeUpdate("CREATE TABLE IF NOT EXISTS product (id INTEGER PRIMARY KEY AUTOINCREMENT, itemName STRING, " +
                    "description STRING, price FLOAT, imagePath STRING)");
            statement.executeUpdate("CREATE TABLE IF NOT EXISTS users (id INTEGER PRIMARY KEY AUTOINCREMENT, nickName STRING," +
                    " email STRING, password STRING, hasSubscribed BOOLEAN)");
            statement.executeUpdate("CREATE TABLE IF NOT EXISTS news (id INTEGER PRIMARY KEY AUTOINCREMENT, title STRING, body STRING)");
            statement.executeUpdate("CREATE TABLE IF NOT EXISTS review (id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    "author STRING, body STRING, fk_IdProduct INTEGER, FOREIGN KEY(fk_IdProduct) REFERENCES product(id))");
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }
    public ShangaDbContext()
    {
        this.databaseDriverLoad();
        this.initConnection();
        this.createTables();
    }
    public ProductItemViewModel getProductById(Integer idComp)
    {
        ProductItemViewModel productItemViewModel = null;
        try {
            Statement statement = connection.createStatement();
            ResultSet sqlResult = statement.executeQuery("SELECT * FROM product");
            while (sqlResult.next())
            {
                if (idComp.equals(sqlResult.getInt("id")))
                    productItemViewModel = new ProductItemViewModel(sqlResult.getInt("id"), sqlResult.getString("itemName"), sqlResult.getString("description")
                            , sqlResult.getString("imagePath")
                            , sqlResult.getFloat("price"));
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return productItemViewModel;
    }

    public ProductItemViewModel getProductByItemName(String itemName)
    {
        ProductItemViewModel productItemViewModel = null;
        try {
            Statement statement = connection.createStatement();
            ResultSet sqlResult = statement.executeQuery("SELECT * FROM product");
            while (sqlResult.next())
            {
                if (sqlResult.getString("itemName").equals(itemName))
                    productItemViewModel = new ProductItemViewModel(sqlResult.getInt("id"), sqlResult.getString("itemName"), sqlResult.getString("description")
                            , sqlResult.getString("imagePath")
                            , sqlResult.getFloat("price"));
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return productItemViewModel;
    }
    public UsersViewModel getUserByEmailPassword(String email, String password)
    {
        UsersViewModel usersViewModel = null;
        try
        {
            Statement statement = connection.createStatement();
            ResultSet sqlResult = statement.executeQuery("SELECT * FROM users");
            while (sqlResult.next())
            {
                if (sqlResult.getString("email").equals(email) && sqlResult.getString("password").equals(password))
                    usersViewModel = new UsersViewModel(sqlResult.getInt("id"), sqlResult.getString("nickName"), sqlResult.getString("email"),
                    sqlResult.getBoolean("hasSubscribed"));
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        if (usersViewModel == null)
            System.err.println("ShangaDbContext.getUserByEmailPassword : No such user " + email);
        return (usersViewModel);
    }
    public UsersViewModel getUserByEmail(String email)
    {
        UsersViewModel usersViewModel = null;
        try
        {
            Statement statement = connection.createStatement();
            ResultSet sqlResult = statement.executeQuery("SELECT * FROM users");
            while (sqlResult.next())
            {
                if (sqlResult.getString("email").equals(email))
                    usersViewModel = new UsersViewModel(sqlResult.getInt("id"), sqlResult.getString("nickName"), sqlResult.getString("email"),
                            sqlResult.getBoolean("hasSubscribed"));
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        if (usersViewModel == null)
            System.err.println("ShangaDbContext.getUserByEmailPassword : No such user " + email);
        return (usersViewModel);
    }
    public List<UsersViewModel> getUsers()
    {
        List<UsersViewModel> usersViewModelList = null;
        try
        {
            usersViewModelList = new LinkedList<UsersViewModel>();
            Statement statement = connection.createStatement();
            ResultSet sqlResult = statement.executeQuery("SELECT * FROM users");
            while (sqlResult.next())
                usersViewModelList.add(new UsersViewModel(sqlResult.getInt("id"), sqlResult.getString("nickName")
                , sqlResult.getString("email"), sqlResult.getBoolean("hasSubscribed")));
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return usersViewModelList;
    }
    public List<ProductItemViewModel> getProducts()
    {
        List<ProductItemViewModel> productList = null;
        try
        {
            productList = new ArrayList<>();

            Statement statement = connection.createStatement();
            ResultSet sqlResult = statement.executeQuery("SELECT * FROM product");
            while (sqlResult.next())
                productList.add(new ProductItemViewModel(sqlResult.getInt("id"), sqlResult.getString("itemName"), sqlResult.getString("description")
                        , sqlResult.getString("imagePath")
                        , sqlResult.getFloat("price")));
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return (productList);
    }
    public List<NewsViewModel> getNews()
    {
        List<NewsViewModel> newsList = null;
        try
        {
            newsList = new ArrayList<NewsViewModel>();

            Statement statement = connection.createStatement();
            ResultSet sqlResult = statement.executeQuery("SELECT * FROM news");
            while (sqlResult.next())
                newsList.add(new NewsViewModel(sqlResult.getString("title"), sqlResult.getString("body")));
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return (newsList);
    }
    public List<ReviewViewModel> getReviewsFromProduct(int productId)
    {
        List<ReviewViewModel> reviewsList = null;
        try
        {
            reviewsList = new LinkedList<ReviewViewModel>();
            PreparedStatement pStmt = connection.prepareStatement("SELECT * FROM review WHERE fk_IdProduct = ?");
            pStmt.setInt(1, productId);
            ResultSet sqlResult = pStmt.executeQuery();
            while (sqlResult.next())
                reviewsList.add(new ReviewViewModel(sqlResult.getInt("fk_IdProduct"), sqlResult.getString("author")
                , sqlResult.getString("body")));
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return (reviewsList);
    }
    public void addProduct(String itemName, String description, float price, String imagePath)
    {
        try
        {
            Statement statement = this.connection.createStatement();
            statement.executeUpdate("INSERT INTO product (itemName, description, price, imagePath) VALUES ('"+ itemName + "'" +
                    ", '" + description + "'" +
                    ", " + price +
                    ", '" + imagePath + "'" +
                    ")");
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }
    public void addReview(ReviewViewModel rvm)
    {
        try
        {
            PreparedStatement pStmt = this.connection.prepareStatement("INSERT INTO review (fk_IdProduct, author, body)" +
                    "VALUES(?, ?, ?)");
            pStmt.setInt(1, rvm.fk_IdProduct);
            pStmt.setString(2, rvm.author);
            pStmt.setString(3, rvm.body);
            pStmt.execute();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }
    public void addNews(String title, String body)
    {
        if (title == null)
        return ;
        if (body == null)
            return ;

        try
        {
            PreparedStatement pStmt = this.connection.prepareStatement("INSERT INTO news (title, body) VALUES (?, ?)");
            pStmt.setString(1, title);
            pStmt.setString(2, body);
            pStmt.execute();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }
    public boolean isAdminLogInValid(String adminNickNew, String adminPassNew)
    {
        if (adminNickNew.equals(ShangaDbContext.shangaAdminNick) && adminPassNew.equals(ShangaDbContext.shangaAdminPass))
            return (true);
        return (false);
    }
    public void addUser(String nickName, String email, String password)
    {
        try
        {
            PreparedStatement pStmt = connection.prepareStatement("INSERT INTO users(nickName, email, password) VALUES(?, ?, ?)");
            pStmt.setString(1, nickName);
            pStmt.setString(2, email);
            pStmt.setString(3, password);
            pStmt.execute();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    public void dropNews()
    {
        try
        {
            Statement st = this.connection.createStatement();
            st.executeUpdate("DROP TABLE IF EXISTS news");
            System.out.println("ShangaDbContext.dropNews: Table news dropped.");
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }
    public void dropReviews()
    {
        try
        {
            Statement st = this.connection.createStatement();
            st.executeUpdate("DROP TABLE IF EXISTS review");
            System.out.println("ShangaDbContext.dropReviews: Table review dropped.");
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }
    public void dropProducts()
    {
        try
        {
            Statement st = this.connection.createStatement();
            st.executeUpdate("DROP TABLE IF EXISTS product");
            System.out.println("ShangaDbContext.dropProducts: Table products dropped.");
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }
    public void dropUsers()
    {
        try
        {
            Statement st = this.connection.createStatement();
            st.executeUpdate("DROP TABLE IF EXISTS users");
            System.out.println("ShangaDbContext.dropUsers: Table users dropped.");
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }
    public void deleteUserByEmail(String currentEmail)
    {
        try
        {
            PreparedStatement pStmt = connection.prepareStatement("DELETE FROM users WHERE email=?");
            pStmt.setString(1, currentEmail);
            pStmt.execute();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }
    public void updateUserFromViewModel(String currentEmail, UsersViewModel newUserModel) throws SQLException
    {
        PreparedStatement pStmt = connection.prepareStatement("UPDATE users SET nickName=?, email=?," +
                "hasSubscribed=? WHERE users.email=?");
        pStmt.setString(1, newUserModel.nickName);
        pStmt.setString(2, newUserModel.email);
        pStmt.setBoolean(3, newUserModel.hasSubscribed);
        pStmt.setString(4, currentEmail);
        pStmt.execute();
    }
    public void updateUserFromViewModel(String currentEmail, String newPassword, UsersViewModel newUserModel)
    {
        try
        {
            if (newPassword.equals(ShangaDbContext.shangaIgnoredPassword))
            {
                this.updateUserFromViewModel(currentEmail, newUserModel);
                return ;
            }
            PreparedStatement pStmt = connection.prepareStatement("UPDATE users SET nickName=?, email=?," +
                        "password=?, hasSubscribed=? WHERE users.email=?");
            pStmt.setString(1, newUserModel.nickName);
            pStmt.setString(2, newUserModel.email);
            pStmt.setString(3, newPassword);
            pStmt.setBoolean(4, newUserModel.hasSubscribed);
            pStmt.setString(5, currentEmail);
            pStmt.execute();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }
    public UsersViewModel updateUserNewsletterStatusByEmail(String subscribed, UsersViewModel userInfo)
    {
        try
        {
            PreparedStatement pStmt = connection.prepareStatement("UPDATE users SET hasSubscribed=? WHERE email=?");
            pStmt.setBoolean(1, !(subscribed == null));
            pStmt.setString(2, userInfo.email);
            pStmt.execute();
            System.out.println("User " + userInfo.nickName  + " has "+ String.valueOf(new Boolean(subscribed == null)) + " to newsletter.");
            userInfo.hasSubscribed = !(subscribed == null);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return (userInfo);
    }
    private static final String shangaIgnoredPassword = "xxxxxx";
    private static final String shangaAdminNick = "admin";
    private static final String shangaAdminPass = "admin";
}
