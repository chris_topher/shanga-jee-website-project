package eu.shanga.common;

/**
 * Created by Christophine on 25/12/2015.
 */
public class AdminSessionInstance {
    boolean connectionValid;
    private static AdminSessionInstance ourInstance = new AdminSessionInstance();

    public static AdminSessionInstance getInstance() {
        return ourInstance;
    }

    private AdminSessionInstance() {
        connectionValid = false;
    }
    public boolean isConnectionValid()
    {
        return connectionValid;
    }
    public void setConnectionValidity(boolean validity)
    {
        this.connectionValid = validity;
    }
}
