package eu.shanga.common;

import eu.shanga.viewModels.UsersViewModel;

/**
 * Created by Christophine on 25/12/2015.
 */
public class SessionInstance {
    private boolean hasInitialized;
    UsersViewModel userInfo;
    private static SessionInstance ourInstance = new SessionInstance();
    public static SessionInstance getInstance() {
        return ourInstance;
    }
    public Boolean knowIfHasInitialized()
    {
        return hasInitialized;
    }
    public UsersViewModel getUserInfo()
    {
        return this.userInfo;
    }
    public void setUserInfo(UsersViewModel uvm)
    {
        hasInitialized = true;
        this.userInfo = uvm;
    }
    private SessionInstance() {
        hasInitialized = false;
    }
}
