package eu.shanga.common;

/**
 * Created by Christophine on 13/12/2015.
 */

public class ControllerBase extends javax.servlet.http.HttpServlet
{
    boolean hasInitialized;
    private ShangaDbContext _dbContext;
    public ControllerBase()
    {
        this.hasInitialized = false;
    }
    public void initialize()
    {
        if (hasInitialized == false)
        {
            this._dbContext = new ShangaDbContext();
            System.out.println("ControllerBase has initialized. New dbContext created.");
            hasInitialized = true;
        }
    }
    public ShangaDbContext getdbContext()
    {
        return this._dbContext;
    }
}
