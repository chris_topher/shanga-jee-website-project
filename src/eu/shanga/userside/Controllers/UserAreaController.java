package eu.shanga.userside.Controllers;

import eu.shanga.common.ControllerBase;
import eu.shanga.common.SessionStatus;
import eu.shanga.viewModels.UsersViewModel;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Christophine on 22/12/2015.
 */

public class UserAreaController extends ControllerBase {
    UsersViewModel userInfo;
    SessionStatus sessionStatus;
    @Override
    public void init()
    {
        sessionStatus = SessionStatus.DidNotLogIn;
        userInfo = new UsersViewModel();
        super.initialize();
    }
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        request.setAttribute("userInfo", userInfo);
        request.setAttribute("sessionStatus", sessionStatus);
        this.getServletContext().getRequestDispatcher("/user_area.jsp").forward(request, response);
    }
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        this.sessionStatus = SessionStatus.None;
        this.userInfo.nickName = request.getParameter("userInfo.nickName");
        this.userInfo.email =  request.getParameter("userInfo.email");
        String subscribed = request.getParameter("subscribed");
        this.userInfo = this.getdbContext().getUserByEmail(this.userInfo.email);
        this.getdbContext().updateUserNewsletterStatusByEmail(subscribed, this.userInfo);
        request.setAttribute("userInfo", userInfo);
        request.setAttribute("sessionStatus", this.sessionStatus);
        this.getServletContext().getRequestDispatcher("/user_area.jsp").forward(request, response);
    }
}
