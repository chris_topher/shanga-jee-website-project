package eu.shanga.userside.Controllers;
/**
 * Created by Christophine on 12/12/2015.
 */

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import eu.shanga.common.ControllerBase;

public class HomeController extends ControllerBase
{
    String title;
    String body;
    @Override
    public void init()
    {
        super.initialize();
    }
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        request.setAttribute("newsList", this.getdbContext().getNews());
        this.getServletContext().getRequestDispatcher("/home.jsp").forward(request, response);
    }
}
