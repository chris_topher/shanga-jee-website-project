package eu.shanga.userside.Controllers;

import eu.shanga.common.ControllerBase;
import eu.shanga.viewModels.ProductItemViewModel;
import eu.shanga.viewModels.ReviewViewModel;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by Christophine on 20/12/2015.
 */
public class ProductController extends ControllerBase
{
    @Override
    public void init()
    {
        super.initialize();
    }
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String itemName = request.getParameter("itemName");
        if (itemName == null)
            request.setAttribute("itemInfo", null);
        else {
            ProductItemViewModel itemInfo = this.getdbContext().getProductByItemName(itemName);
            List<ReviewViewModel> reviewViewModels = this.getdbContext().getReviewsFromProduct(itemInfo.id);
            request.setAttribute("itemInfo", itemInfo);
            request.setAttribute("reviewsList", reviewViewModels);
        }
        this.getServletContext().getRequestDispatcher("/product.jsp").forward(request, response);
    }
    private boolean validateUserReview(ReviewViewModel rvm)
    {
        if (rvm.author == null ||rvm.author.equals(""))
        {
            System.err.println("ProductController.validateUserReview : Null or empty string review.author");
            return false;
        }
        if (rvm.body == null ||rvm.body.equals(""))
        {
            System.err.println("ProductController.validateUserReview : Null or empty string review.body");
            return false;
        }
        return true;
    }
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        Integer fk_IdProduct = Integer.valueOf(request.getParameter("fk_IdProduct"));
        String author = request.getParameter("review_author");
        String body = request.getParameter("review_body");
        ReviewViewModel reviewViewModel = new ReviewViewModel(fk_IdProduct, author, body);
        if (this.validateUserReview(reviewViewModel))
            this.getdbContext().addReview(reviewViewModel);
        ProductItemViewModel itemInfo = this.getdbContext().getProductById(fk_IdProduct);
        List<ReviewViewModel> reviewViewModels = this.getdbContext().getReviewsFromProduct(fk_IdProduct);
        request.setAttribute("itemInfo", itemInfo);
        request.setAttribute("reviewsList", reviewViewModels);
        this.getServletContext().getRequestDispatcher("/product.jsp").forward(request, response);
    }
}
