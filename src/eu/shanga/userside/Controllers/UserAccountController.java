package eu.shanga.userside.Controllers;

import eu.shanga.common.ControllerBase;
import eu.shanga.common.SessionInstance;
import eu.shanga.viewModels.UsersViewModel;
import eu.shanga.common.SessionStatus;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Christophine on 15/12/2015.
 */
public class UserAccountController extends ControllerBase
{
    UsersViewModel currentUser;
    boolean hasLoggedIn;
    SessionStatus sessionStatus;
    @Override
    public void init()
    {
        sessionStatus = SessionStatus.None;
        super.initialize();
        hasLoggedIn = false;
    }
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        req.setAttribute("sessionStatus",sessionStatus);
        req.setAttribute("hasLoggedIn", new Boolean(hasLoggedIn));
        if (hasLoggedIn == false)
            this.getServletContext().getRequestDispatcher("/login_register.jsp").forward(req, resp);
        else
            this.getServletContext().getRequestDispatcher("/user_area.jsp").forward(req, resp);
    }
    protected void logIn(HttpServletRequest req, HttpServletResponse resp, String email, String password)
            throws ServletException, IOException
    {
        UsersViewModel userInfo = this.getdbContext().getUserByEmailPassword(email, password);
        if (userInfo == null) //No such user found
        {
            this.sessionStatus = SessionStatus.UserDoesNotExist;
            this.hasLoggedIn = false;
            req.setAttribute("sessionStatus",sessionStatus);
            req.setAttribute("hasLoggedIn", new Boolean(hasLoggedIn));
            this.getServletContext().getRequestDispatcher("/login_register.jsp").forward(req, resp);
        }
        else // User found
        {
            this.sessionStatus = SessionStatus.None;
            this.hasLoggedIn = true;
            req.setAttribute("sessionStatus",sessionStatus);
            req.setAttribute("hasLoggedIn", new Boolean(hasLoggedIn));
            req.setAttribute("userInfo", userInfo);
            SessionInstance.getInstance().setUserInfo(userInfo);
            this.getServletContext().getRequestDispatcher("/user_area.jsp").forward(req, resp);
        }
    }
    public void registerUser(HttpServletRequest req, HttpServletResponse resp,
                             String nickname, String email, String password)
            throws ServletException, IOException
    {
        this.getdbContext().addUser(nickname, email, password);
        this.sessionStatus = SessionStatus.RegisterSuccess;
        this.hasLoggedIn = false;
        req.setAttribute("sessionStatus",sessionStatus);
        req.setAttribute("hasLoggedIn", new Boolean(hasLoggedIn));
        this.getServletContext().getRequestDispatcher("/login_register.jsp").forward(req, resp);
    }
    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        String nickname = req.getParameter("nickname");
        String email = req.getParameter("email");
        String password = req.getParameter("password");
        if (nickname == null) //Means a LogIn
            this.logIn(req, resp, email, password);
        else //Means a registration
            this.registerUser(req, resp, nickname, email, password);
    }
}
