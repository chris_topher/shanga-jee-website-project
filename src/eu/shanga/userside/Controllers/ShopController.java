package eu.shanga.userside.Controllers;

import eu.shanga.common.ControllerBase;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;

/**
 * Created by Christophine on 14/12/2015.
 */
public class ShopController extends ControllerBase
{
    @Override
    public void init()
    {
        super.initialize();
    }
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        request.setAttribute("productList", this.getdbContext().getProducts());
        this.getServletContext().getRequestDispatcher("/shop.jsp").forward(request, response);
    }
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {

    }
}
