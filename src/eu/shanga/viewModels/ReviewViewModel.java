package eu.shanga.viewModels;

/**
 * Created by Christophine on 26/12/2015.
 */
public class ReviewViewModel
{
    public ReviewViewModel(int fk_IdProduct, String author, String body)
    {
        this.fk_IdProduct = fk_IdProduct;
        this.author = author;
        this.body = body;
    }
    public Integer fk_IdProduct;
    public String author;
    public String body;
}
