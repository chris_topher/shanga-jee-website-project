package eu.shanga.viewModels;

/**
 * Created by Christophine on 15/12/2015.
 */
public class UsersViewModel {
    public UsersViewModel()
    {
        this.id = 0;
        this.nickName = "";
        this.email = "";
        this.hasSubscribed = false;
    }
    public UsersViewModel(int id, String nickName, String email, boolean hasSubscribed)
    {
        this.id = id;
        this.nickName = nickName;
        this.email = email;
        this.hasSubscribed = hasSubscribed;
    }
    public String getCheckboxStatus()
    {
        if (this.hasSubscribed.equals(true))
            return ("checked");
        return ("");
    }
    public Integer id;
    public String nickName;
    public String email;
    public Boolean hasSubscribed;
}
