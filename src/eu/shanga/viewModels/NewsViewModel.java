package eu.shanga.viewModels;

/**
 * Created by Christophine on 14/12/2015.
 */
public class NewsViewModel {
    public NewsViewModel(String title, String body)
    {
        this.title = title;
        this.body = body;
    }
    public String title;
    public String body;
}
