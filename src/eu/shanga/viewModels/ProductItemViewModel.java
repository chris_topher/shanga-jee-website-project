package eu.shanga.viewModels;

/**
 * Created by Christophine on 14/12/2015.
 */
public class ProductItemViewModel
{
    public ProductItemViewModel(int id, String itemName, String description,
                                String imagePath, float price)
    {
        this.id = id;
        this.itemName = itemName;
        this.description = description;
        this.imagePath = imagePath;
        this.price = price;
    }
    public Integer id;
    public String itemName;
    public String description;
    public String imagePath;
    public Float  price;
}
