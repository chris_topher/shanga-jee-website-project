package eu.shanga.adminside.Controllers;

import eu.shanga.common.AdminSessionInstance;
import eu.shanga.common.ControllerBase;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Christophine on 23/12/2015.
 */
public class AdminController extends ControllerBase
{
    boolean isConnectionValid;
    @Override
    public void init()
    {
        this.isConnectionValid = false;
        super.initialize();
    }
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        request.setAttribute("isConnectionValid", new Boolean(this.isConnectionValid));
        this.getServletContext().getRequestDispatcher("/admin_log.jsp").forward(request, response);
    }
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String admin_nick = request.getParameter("admin_nick");
        String admin_pass = request.getParameter("admin_pass");
        if (this.getdbContext().isAdminLogInValid(admin_nick, admin_pass) == true)
        {
            this.isConnectionValid = true;
            request.setAttribute("isConnectionValid", new Boolean(this.isConnectionValid));
            AdminSessionInstance.getInstance().setConnectionValidity(this.isConnectionValid);
            request.setAttribute("message", "");
            request.setAttribute("formerTitle", "");
            request.setAttribute("formerBody", "");
            this.getServletContext().getRequestDispatcher("/admin_write.jsp").forward(request, response);
        }
        request.setAttribute("isConnectionValid", new Boolean(this.isConnectionValid));
        this.getServletContext().getRequestDispatcher("/admin_log.jsp").forward(request, response);
    }
}
