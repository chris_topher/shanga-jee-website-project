package eu.shanga.adminside.Controllers;

import eu.shanga.common.AdminSessionInstance;
import eu.shanga.common.ControllerBase;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Christophine on 25/12/2015.
 */
public class WriteController extends ControllerBase
{
    @Override
    public void init()
    {
        super.initialize();
    }
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        if (AdminSessionInstance.getInstance().isConnectionValid() == true)
        {
            request.setAttribute("message", "");
            request.setAttribute("formerTitle", "");
            request.setAttribute("formerBody", "");
            this.getServletContext().getRequestDispatcher("/admin_write.jsp").forward(request, response);
        }
        else
            this.getServletContext().getRequestDispatcher("/admin_error.jsp").forward(request, response);
    }
    private void dropIfAsked(HttpServletRequest request, HttpServletResponse response, String dropNews)
            throws ServletException, IOException
    {
        if (dropNews != null) {
            request.setAttribute("message", "Vous avez, avec succès, supprimé tous les articles.");
            request.setAttribute("formerTitle", "");
            request.setAttribute("formerBody", "");
            this.getdbContext().dropNews();
            this.getServletContext().getRequestDispatcher("/admin_write.jsp").forward(request, response);
        }
    }
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String dropNews = request.getParameter("dropNews");
        String message = "";
        String title = request.getParameter("article_title");
        String body = request.getParameter("article_body");
        this.dropIfAsked(request, response, dropNews);
        if (title != null && body != null)
        {
            this.getdbContext().addNews(title, body);
            message = "Vous avez posté votre article avec succès !";
        }
        if (title.equals(""))
            message = "Vous n'avez pas fourni de titre.";
        if (body.equals(""))
            message = "Vous n'avez pas fourni de corps d'article.";
        request.setAttribute("message", message);
        request.setAttribute("formerTitle", title != null ? title : "");
        request.setAttribute("formerBody", body != null ? body : "");
        this.getServletContext().getRequestDispatcher("/admin_write.jsp").forward(request, response);
    }
}
