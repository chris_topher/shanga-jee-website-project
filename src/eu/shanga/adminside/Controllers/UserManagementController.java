package eu.shanga.adminside.Controllers;

import eu.shanga.common.AdminSessionInstance;
import eu.shanga.common.ControllerBase;
import eu.shanga.viewModels.UsersViewModel;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by Christophine on 25/12/2015.
 */
public class UserManagementController extends ControllerBase {
    @Override
    public void init()
    {
        super.initialize();
    }
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        if (AdminSessionInstance.getInstance().isConnectionValid())
        {
            List<UsersViewModel> usersList = this.getdbContext().getUsers();
            request.setAttribute("usersList", usersList);
            request.setAttribute("successMessage", "");
            this.getServletContext().getRequestDispatcher("/admin_manage_users.jsp").forward(request, response);
        }
        else
            this.getServletContext().getRequestDispatcher("/admin_error.jsp").forward(request, response);
    }
    private boolean validateAdminRequest(String newPassword, UsersViewModel uvm)
    {
        if (newPassword == null || newPassword.equals(""))
        {
            System.err.println("Password equals null");
            return (false);
        }
        if (uvm.nickName == null ||uvm.nickName.equals(""))
        {
            System.err.println("uvm nick equals null");
            return false;
        }
        if (uvm.email == null ||uvm.email.equals(""))
        {
            System.err.println("Password equals null");
            return false;
        }
        return true;
    }
    private void handleUserDelete(String userEmail, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        this.getdbContext().deleteUserByEmail(userEmail);
        List<UsersViewModel> usersList = this.getdbContext().getUsers();
        request.setAttribute("usersList", usersList);
        request.setAttribute("successMessage", "Utilisateur supprimé avec succès !");
        this.getServletContext().getRequestDispatcher("/admin_manage_users.jsp").forward(request, response);
    }
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        System.out.println("deleteThisUser = " + request.getParameter("deleteThisUser"));
        if (request.getParameter("deleteThisUser") != null)
            this.handleUserDelete(request.getParameter("current_email"), request, response);
        String successMessage;
        String currentEmail = request.getParameter("current_email");
        String newPassword = request.getParameter("new_password");
        UsersViewModel newUserModel = new UsersViewModel();
        newUserModel.nickName = request.getParameter("new_nickname");
        newUserModel.email = request.getParameter("new_email");
        newUserModel.hasSubscribed = !(request.getParameter("new_newsletter_status") == null);
        if (this.validateAdminRequest(newPassword, newUserModel) == true)
        {
            this.getdbContext().updateUserFromViewModel(currentEmail, newPassword, newUserModel);
            successMessage = "Utilisateur mis à jour avec succès !";
        }
        else
            successMessage = "Vérifiez vos entrées.";
        List<UsersViewModel> usersList = this.getdbContext().getUsers();
        request.setAttribute("usersList", usersList);
        request.setAttribute("successMessage", successMessage);
        this.getServletContext().getRequestDispatcher("/admin_manage_users.jsp").forward(request, response);
    }
}
