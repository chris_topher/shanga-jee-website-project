# README #


### What is this repository for? ###

This repository is storing source for a scholar Java EE/Tomcat project I have worked on with my good mate Romain CH.

#### Server

Tomcat

#### Languages

- Java :coffee:
- HTML/JSP

#### Database

Sqlite

It consists mainly on Java and HTML code.

The main theme is *mangas*.

### How do I get set up? ###

* Clone the repo.
* Download Tomcat
* Using Intellij IDEA, Follow the steps in the powerpoint intellijTomcat.ppt to create a new runtime configuration
* Run the app

### Team ###
* Christopher Jdl. (Christophine)
* Romain CH (Jallor)