<%--
  Created by IntelliJ IDEA.
  User: Christophine
  Date: 23/12/2015
  Time: 18:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Admin - Area</title>
    <%@include file="/csslink.jsp"%>
</head>
<body>
<%@include file="/navbar_admin.jsp"%>
<% String message = (String)request.getAttribute("message");%>
<% String formerTitle = (String)request.getAttribute("formerTitle");%>
<% String formerBody = (String)request.getAttribute("formerBody"); %>
<% boolean postSuccess = message.contains("succès");%>
<h4>Écrire un article</h4>
<% if (postSuccess) {%>
    <p class="green"><%=message%></p>
<% }%>
<% if (!postSuccess) {%>
    <p class="red"><%=message%></p>
<% }%>
<div class="row">
<form action="/Admin/Write" class="col s12" method="post">
    <div class="row">
        <div class="input-field col s6">
            <input value="<%=formerTitle%>" id="article_title" type="text" name="article_title" class="validate">
            <label for="article_title">Title</label>
        </div>
        <div class="input-field col s12">
            <textarea id="article_body" name="article_body" class="materialize-textarea"><%=formerBody%></textarea>
            <label for="article_body">Body</label>
        </div>
    </div>
    <button class="btn waves-effect waves-light" type="submit" name="action"><i class="material-icons right">send</i> Poster l'article
    </button>
</form>
    <form action="/Admin/Write" class="col s12" method="post">
        <input type="hidden" value="dropNews" name="dropNews">
        <button class="btn waves-effect waves-light red" type="submit" name="action"><i class="material-icons right">delete</i> Supprimer tous les articles
        </button>
    </form>
</div>
</body>
</html>
