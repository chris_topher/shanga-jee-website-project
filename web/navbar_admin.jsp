<%--
  Created by IntelliJ IDEA.
  User: Christophine
  Date: 23/12/2015
  Time: 16:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<nav>
    <div class="nav-wrapper grey darken-4">
        <a href="/" class="brand-logo" >Shanga Admin</a>
        <ul id="nav-mobile" class="right hide-on-med-and-down">
            <li><a class="waves-effect waves-light btn grey darken-4" href="/Admin/Write">Write News</a></li>
            <li><a class="waves-effect waves-light btn grey darken-4" href="/Admin/Manage">Manage Users</a></li>
            <li><a class="waves-effect waves-light btn grey darken-4" href="/">Get Out</a></li>
        </ul>
    </div>
</nav>
