<%@ page import="eu.shanga.viewModels.UsersViewModel" %>
<%@ page import="eu.shanga.common.SessionStatus" %>
<%@ page import="eu.shanga.common.SessionInstance" %><%--
  Created by IntelliJ IDEA.
  User: Christophine
  Date: 22/12/2015
  Time: 19:39
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Shanga - User Area</title>
    <%@include file="csslink.jsp"%>
</head>
<body>
<%@include file="navbar.jsp" %>
<% UsersViewModel userInfo = (UsersViewModel) request.getAttribute("userInfo");%>
<% SessionStatus sessionStatus = (SessionStatus) request.getAttribute("sessionStatus");%>
<% if (userInfo != null && !sessionStatus.equals(SessionStatus.DidNotLogIn)) {%>
<h3>Bienvenue, <%= userInfo.nickName %> !</h3>
<!-- Switch -->
<p>Voulez-vous vous inscrire à notre newsletter ? :)</p><br/>
<form action="/Area/" method="post">
    <div class="switch">
        <input type="hidden" value="<%=userInfo.nickName%>" name="userInfo.nickName">
        <input type="hidden" value="<%=userInfo.email%>" name="userInfo.email">
        <label>
            No
            <input name="subscribed" type="checkbox" <%= userInfo.getCheckboxStatus()%>>
            <span class="lever red"></span>
            Yes
        </label>
    </div>
    <button class="btn waves-effect waves-light" type="submit" name="action"><i class="material-icons right">send</i> Enregistrer les modifications
    </button>
</form>
<%}%>
<% if (SessionInstance.getInstance().knowIfHasInitialized().equals(false)){%>
    <h1>Internal Server Error.</h1>
    <blockquote> You should not be here !</blockquote>
<%}%>
<% if (sessionStatus.equals(SessionStatus.DidNotLogIn) && SessionInstance.getInstance().knowIfHasInitialized().equals(true)) {%>
<% userInfo = SessionInstance.getInstance().getUserInfo();%>
<h3>Welcome, <%= userInfo.nickName %> !</h3>
<!-- Switch -->
<p>Do you want to subscribe to our tremendously cool newsletter ? :)</p><br/>
<form action="/Area/" method="post">
    <div class="switch">
        <input type="hidden" value="<%=userInfo.nickName%>" name="userInfo.nickName">
        <input type="hidden" value="<%=userInfo.email%>" name="userInfo.email">
        <label>
            No
            <input name="subscribed" type="checkbox" <%= userInfo.getCheckboxStatus()%>>
            <span class="lever red"></span>
            Yes
        </label>
    </div>
    <button class="btn waves-effect waves-light" type="submit" name="action"><i class="material-icons right">send</i> Enregistrer les modifications
    </button>
</form>
<%}%>
</body>
</html>
