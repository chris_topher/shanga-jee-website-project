<%@ page import="eu.shanga.viewModels.NewsViewModel" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ListIterator" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.util.Collections" %>
<%--
  Created by IntelliJ IDEA.
  User: Christophine
  Date: 12/12/2015
  Time: 16:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>Shanga</title>
    <%@include file="csslink.jsp"%>
  </head>
  <body>
  <%@include file="navbar.jsp" %>
  <% List<NewsViewModel> newsList = (List<NewsViewModel>)request.getAttribute("newsList"); %>
  <% Collections.reverse(newsList);%>

  <ul>
<% if (newsList != null) {%>
  <% for (NewsViewModel elem : newsList){%>
    <li class="shanga-news card-panel hoverable col s12 m6">
      <h3><%= elem.title%></h3>
          <p><%= elem.body %></p>
    </li>
    <%}
    }%>
  </ul>

  </body>
</html>
