<%@ page import="com.sun.org.apache.xpath.internal.operations.Bool" %>
<%@ page import="eu.shanga.viewModels.UsersViewModel" %>
<%@ page import="eu.shanga.common.SessionStatus" %>
<%--
  Created by IntelliJ IDEA.
  User: Christophine
  Date: 15/12/2015
  Time: 18:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Shanga - Account</title>
    <%@include file="csslink.jsp"%>
</head>
<body>
<%@include file="navbar.jsp" %>
<% Boolean hasLoggedIn = (Boolean)request.getAttribute("hasLoggedIn");
    SessionStatus sessionStatus = (SessionStatus)request.getAttribute("sessionStatus");%>
    <% if (hasLoggedIn.equals(false)) {%>
    <% if (!sessionStatus.equals(SessionStatus.None)) {%>
    <span class="text <%= sessionStatus._displayColor %>"> <%= sessionStatus._displayString %> </span>
    <% } %>

<div class="card shanga-account shanga-connect">
    <p>Vous avez déjà un compte ?</p>
    <h4>Connectez-vous</h4>
    <div id="log-in" class="row">
        <form action="/Account/" class="col s6" method="post">
            <div class="row">
                <div class="input-field col s12">
                    <input name="email" id="email" type="email" class="validate">
                    <label for="email">Email</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <input name="password" id="password" type="password" class="validate">
                    <label for="password">Password</label>
                </div>
            </div>
            <button class="btn waves-effect waves-light" type="submit" name="action"><i class="material-icons right">send</i> Connexion
            </button>
        </form>
    </div>
</div>
<div class="card shanga-account shanga-register">
    <p>Vous n'avez pas de compte ?</p>
    <h4>Enregistrez-vous</h4>
    <div id="register" class="row">
        <form action="/Account/" class="col s6" method="post">
            <div class="row">
                <div class="input-field col s12">
                    <input name="nickname" id="nickname_register" type="text" class="validate">
                    <label for="nickname_register">NickName</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <input name="email" id="email_register" type="email" class="validate">
                    <label for="email">Email</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <input name="password" id="password_register" type="password" class="validate">
                    <label for="password">Password</label>
                </div>
            </div>
            <button class="btn waves-effect waves-light" type="submit" name="action"><i class="material-icons right">send</i> Je m'enregistre !
            </button>
        </form>
    </div>
</div>
    <%} //end if hasLoggedIn == false%>
    <% if(hasLoggedIn.equals(true)) {%>
        <% UsersViewModel userInfo = (UsersViewModel)request.getAttribute("userInfo"); %>
        <h1>Internal Server Error.</h1>
        <blockquote>You should not be here !</blockquote>
    <%}%>

</body>
</html>
