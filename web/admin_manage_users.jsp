<%@ page import="java.util.List" %>
<%@ page import="eu.shanga.viewModels.UsersViewModel" %><%--
  Created by IntelliJ IDEA.
  User: Christophine
  Date: 25/12/2015
  Time: 22:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Admin - Manage Users</title>
    <%@include file="/csslink.jsp"%>
</head>
<body>
<%@include file="/navbar_admin.jsp"%>
<% List<UsersViewModel> usersList = (List<UsersViewModel>)request.getAttribute("usersList");
    String successMessage = (String)request.getAttribute("successMessage");
%>
<% if (successMessage.contains("succès")) {%>
<span class="green-text"> <%=successMessage %> </span>
<%}%>
<% if (!successMessage.contains("succès")) {%>
<span class="red-text"><%=successMessage%></span>
<%}%>
<% for (UsersViewModel user : usersList) {%>
<div class="row">

    <div class="col s12 m6">
        <div class="card grey darken-4">
            <form action="/Admin/Manage" method="post">
            <div class="card-content white-text">
                <span class="card-title"><%=user.nickName%>  <span class="badge"><%=user.id %></span></span>
                <!-- MODIFY USER FORM -->
                <div class="row">
                    <div class="input-field col s6">
                        <input type="hidden" value="<%=user.email%>" name="current_email">
                        <input value="<%=user.nickName %>" id="new_nickname" type="text" name="new_nickname" class="validate">
                        <label for="new_nickname">Nom d'utilisateur</label>
                    </div>
                    <div class="input-field col s6">
                        <input value="<%= user.email%>" id="new_email" type="email" name="new_email" class="validate">
                        <label for="new_email">E-mail</label>
                    </div>
                    <div class="input-field col s6">
                        <input value="xxxxxx" id="new_password" type="password" name="new_password" class="validate">
                        <label for="new_password">Mot de passe</label>
                    </div>
                </div>
                <!-- CHECKBOX -->
                <p>
                    <input type="checkbox" class="filled-in" id="new_newsletter_status<%=user.id%>"
                           name="new_newsletter_status" <%=user.getCheckboxStatus()%>/>
                    <label for="new_newsletter_status<%=user.id%>">Forcer l'inscription à la newsletter</label>
                </p>
                <!-- END CHECKBOX -->
                <!-- END MODIFY-->
            </div>
            <div class="card-action">
                <button href="#" class="btn waves-effect waves-light teal lighten-1" type="submit"><i class="material-icons right">send</i> Envoyer les changements</button>
                <form action="/Admin/Manage" method="post" id="deleteThisUser">
                    <input type="hidden" value="<%=user.email%>" name="current_email">
                    <button class="btn waves-effect waves-light red" type="submit" name="deleteThisUser"><i class="material-icons right">delete</i>
                    Supprimer cet utilisateur
                </button>
                </form>
            </div>
            </form>
        </div>
    </div>
</div>
<% }%>
</body>
</html>
