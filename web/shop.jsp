<%@ page import="java.util.List" %>
<%@ page import="eu.shanga.viewModels.ProductItemViewModel" %><%--
  Created by IntelliJ IDEA.
  User: Christophine
  Date: 14/12/2015
  Time: 18:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Shanga - Shop</title>
    <%@include file="csslink.jsp"%>
</head>
<body>
<%@include file="navbar.jsp" %>
   <blockquote> Welcome to the shop, bro !</blockquote>

<% List<ProductItemViewModel> productList = (List<ProductItemViewModel>)request.getAttribute("productList");%>
<p>Total products <%=productList.size()%></p>
<ul>
    <%for (ProductItemViewModel elem : productList) {%>

    <li class="shanga-shop-case card-panel hoverable col s12 m6">
            <div class="shanga-shop-img">
                <img src="<%=elem.imagePath%>"  width="420" height="593">
            </div>
        <a href="/Product/?itemName=<%=elem.itemName%>">
        <h4><%=elem.itemName%></h4>
        <h5><%=elem.price%> €</h5>
        <p><%=elem.description%></p>
        </a>
    </li>
    <% } %>
</ul>
</body>
</html>
