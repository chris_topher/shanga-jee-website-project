<%--
  Created by IntelliJ IDEA.
  User: Christophine
  Date: 25/12/2015
  Time: 18:35
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Shanga - Admin Error</title>
    <%@include file="/csslink.jsp"%>
</head>
<body>
<%@include file="navbar_admin_login.jsp"%>
    <h3>You cannot access this page.</h3>
    <blockquote>You can go back to home page <a href="/">here</a>.</blockquote>
</body>
</html>
