<%@ page import="eu.shanga.viewModels.ProductItemViewModel" %>
<%@ page import="eu.shanga.viewModels.ReviewViewModel" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: Christophine
  Date: 20/12/2015
  Time: 17:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Shanga - Item</title>
    <%@include file="csslink.jsp"%>
</head>
<body>
<%@include file="navbar.jsp" %>
<% ProductItemViewModel itemInfo = (ProductItemViewModel)request.getAttribute("itemInfo"); %>
<% if(itemInfo == null){%>
<p>Resource error : No such product or no product specified.</p>
<% }%>
<%if (itemInfo != null) {%>
<div class="shanga-product card">
    <h1 class="card-title"><%=itemInfo.itemName%></h1>
    <center><div class="">
        <img src="<%=itemInfo.imagePath%>" width="420" height="593" />
    </div></center>
    <h4><%=itemInfo.price%> €</h4>
    <div class="card-content"><p><%=itemInfo.description%></p></div>
    <div class="card-action">
    <button class="btn waves-effect waves-light" type="submit" name="action"><i class="material-icons right">shopping_cart</i> Acheter
    </button>
    </div>
</div>
<!-- REVIEW LIST -->
<% List<ReviewViewModel> reviewViewModels = (List<ReviewViewModel>)request.getAttribute("reviewsList");%>
<% if (reviewViewModels != null) {
 if (!reviewViewModels.isEmpty()) {%>
<h4>Commentaires</h4>
<% for(ReviewViewModel review : reviewViewModels) { %>
    <h5> <%=review.author%> </h5>
    <blockquote><%=review.body%></blockquote>
<%      }
    }
}%>
<!-- END REVIEW LIST -->
<!-- BEGIN COMMENT SECTION-->
<p>Postez un commentaire !</p>
<form accept-charset="UTF-8" method="post" action="/Product/">
<div class="row">

        <div class="input-field col s6">
        <input type="hidden" value="<%=itemInfo.id%>" name="fk_IdProduct">
        </div>
        <div class="input-field col s6">
        <input value="" id="review_author" type="text" name="review_author" class="validate">
            <label for="review_author">Auteur</label>
        </div>
        <div class="input-field col s12">
        <textarea id="review_body" name="review_body" class="materialize-textarea"></textarea>
            <label for="review_body">Commentaire</label>
        </div>
</div>
    <button class="btn waves-effect waves-light" type="submit" name="action"><i class="material-icons right">send</i>Poster le commentaire !
    </button>
</form>
<!-- END COMMENT SECTION-->
<% }%>
</body>
</html>
