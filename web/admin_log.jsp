<%--
  Created by IntelliJ IDEA.
  User: Christophine
  Date: 23/12/2015
  Time: 17:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%Boolean isConnectionValid = (Boolean)request.getAttribute("isConnectionValid");%>
<html>
<head>
    <%if (isConnectionValid.equals(true)){%>
    <meta http-equiv="refresh" content="0; url=/Admin/Write" />
    <%}%>
    <title>Admin - LogIn</title>
    <%@include file="/csslink.jsp"%>
</head>
<body>
    <%@include file="navbar_admin_login.jsp"%>
    <%if (isConnectionValid.equals(false)) {%>
    <h4>Entrez les identifiants Administrateur :</h4>
    <div id="log-in" class="row">
        <form action="/Admin/" class="col s6" method="post">
            <div class="row">
                <div class="input-field col s12">
                    <input name="admin_nick" id="admin_nick" type="text" class="validate">
                    <label for="admin_nick">Admin Nick</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <input name="admin_pass" id="admin_pass" type="password" class="validate">
                    <label for="admin_pass">Password</label>
                </div>
            </div>
            <button class="btn waves-effect waves-light" type="submit" name="action"><i class="material-icons right">send</i> Connexion
            </button>
        </form>
    </div>
    <%}%>
    <%if (isConnectionValid.equals(true)){%>
    <p>Nous vous redirigeons vers le panneau d'administration...</p>
    <%}%>
</body>
</html>
